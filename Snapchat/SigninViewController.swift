//
//  SigninViewController.swift
//  Snapchat
//
//  Created by Abdullah Al-Ahmadi on 1/31/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class SigninViewController: UIViewController {

  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
  }

  @IBAction func signInTapped(_ sender: Any) {
    FIRAuth.auth()?.signIn(withEmail: emailTextField.text!, password: passwordTextField.text!, completion: { (user, error) in
      if error == nil{ // Email registred before
        self.performSegue(withIdentifier: "signInSegue", sender: nil)
      }else{ // Email not registred before
        FIRAuth.auth()?.createUser(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!, completion: { (user, error) in
          if error == nil {
            print("User created and signed in")
            FIRDatabase.database().reference().child("users").child(user!.uid).child("email").setValue(user!.email)
            self.performSegue(withIdentifier: "signInSegue", sender: nil)
          }else{
            print("Could not create a user \(error)")
          }
        })
      }
    })
  }

}

