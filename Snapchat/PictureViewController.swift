//
//  PictureViewController.swift
//  Snapchat
//
//  Created by Abdullah Al-Ahmadi on 1/31/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit
import FirebaseStorage

class PictureViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var imageDescription: UITextField!
  @IBOutlet weak var nextButton: UIButton!
  
  var imagePicker = UIImagePickerController()

  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    imagePicker.delegate = self
  }
  
  @IBAction func cameraTapped(_ sender: Any) {
    imagePicker.sourceType = .photoLibrary
    imagePicker.allowsEditing = false
    present(imagePicker, animated: true, completion: nil)
  }
  @IBAction func nextButtonTapped(_ sender: Any) {
    
    nextButton.isEnabled = false
    let imageFolder = FIRStorage.storage().reference().child("images")
    let imageData = UIImageJPEGRepresentation(imageView.image!, 0.1)!
    
    imageFolder.child("\(NSUUID().uuidString).jpg").put(imageData, metadata: nil) { (metadata, error) in
      print("We are trying to upload")
      if error != nil {
        print("We have an error \(error)")
      }
      else {
        print("Image uploaded succesfully")
        self.performSegue(withIdentifier: "selectUserSegue", sender: metadata?.downloadURL()!.absoluteString)
      }
    }
    
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let nextVC = segue.destination as! SelectUserViewController
    nextVC.imageURL = sender as! String
    nextVC.descript = imageDescription.text!
    
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    let image = info[UIImagePickerControllerOriginalImage] as! UIImage
    imageView.image = image
    imageView.backgroundColor = UIColor.clear
    imagePicker.dismiss(animated: true, completion: nil)
  }
  
}

