//
//  SnapsViewController.swift
//  Snapchat
//
//  Created by Abdullah Al-Ahmadi on 1/31/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class SnapsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet weak var registeredUserSnapsList: UITableView!
  var snaps : [Snap] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    registeredUserSnapsList.dataSource = self
    registeredUserSnapsList.delegate = self
    
    FIRDatabase.database().reference().child("users").child(FIRAuth.auth()!.currentUser!.uid).child("snaps").observe(FIRDataEventType.childAdded, with: {(snapshot) in
      
      let snap = Snap()
      let snapValue = snapshot.value as! NSDictionary
      
      snap.imageURL = snapValue["imageURL"] as! String
      snap.from = snapValue["from"] as! String
      snap.descript = snapValue["description"] as! String
    
      
      // populate the list with users
      
      self.snaps.append(snap)
      self.registeredUserSnapsList.reloadData()
      
    })
    
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return snaps.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    let snapSender = snaps[indexPath.row]
    cell.textLabel?.text = snapSender.from
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let snap = snaps[indexPath.row]
    
    performSegue(withIdentifier: "viewSelectedSnapSegue", sender: snap)
    
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "viewSelectedSnapSegue" {
      let nextVC = segue.destination as! ViewSnapViewController
      nextVC.selectedSnap = sender as! Snap
    }
    
  }
  
  @IBAction func logoutUser(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
}
