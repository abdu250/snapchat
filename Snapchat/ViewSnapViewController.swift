//
//  ViewSnapViewController.swift
//  Snapchat
//
//  Created by Abdullah Al-Ahmadi on 2/2/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit
import SDWebImage

class ViewSnapViewController: UIViewController {

  @IBOutlet weak var snapViewVCImageview: UIImageView!
  @IBOutlet weak var snapViewVCTextLabel: UILabel!
  
  var selectedSnap = Snap()
   
    override func viewDidLoad() {
        super.viewDidLoad()

        snapViewVCTextLabel.text = selectedSnap.descript
        snapViewVCImageview.sd_setImage(with: URL(string: selectedSnap.imageURL))
    }
  
  
}
