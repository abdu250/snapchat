//
//  SelectUserViewController.swift
//  Snapchat
//
//  Created by Abdullah Al-Ahmadi on 1/31/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit
import FirebaseDatabase

class SelectUserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet weak var usersTableView: UITableView!
  var users : [User] = []
  
  var imageURL = ""
  var descript = ""
  
  override func viewDidLoad() {
    super.viewDidLoad()
    usersTableView.dataSource = self
    usersTableView.delegate = self
    
    FIRDatabase.database().reference().child("users").observe(FIRDataEventType.childAdded, with: {(snapshot) in
    
      let usersInTheList = User()
      usersInTheList.email = (snapshot.value as! NSDictionary) ["email"] as! String
      usersInTheList.uid = snapshot.key
      
      // populate the list with users
      
      self.users.append(usersInTheList)
      self.usersTableView.reloadData()
      
    })
    
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return users.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell =  UITableViewCell()
    let user  = users[indexPath.row]
    
    cell.textLabel?.text = user.email
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let user  = users[indexPath.row]
    let snap = ["from":user.email, "description":descript, "imageURL":imageURL]
    
    FIRDatabase.database().reference().child("users").child(user.uid).child("snaps").childByAutoId().setValue(snap)
    navigationController!.popToRootViewController(animated: true)
  }
  
}
